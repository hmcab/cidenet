<!DOCTYPE html>
<html lang="es">
    <head>        
        <title>Cidenet</title>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    </head>
    <body>
        <div class="container">
            <div class="row top-space">                
                <div class="center-block alert alert-warning">
                    P&aacute;gina solicitada no existe.
                    Para regresar al inicio pulse 
                    <a href="/Cidenet/index.jsp">aqu&iacute;</a>.
                </div>
            </div>            
        </div>
    </body>
</html>