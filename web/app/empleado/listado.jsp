<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/include/header.jsp" />

<div class="col-lg-9 centered">
    <h1 class="display-3 bg-info text-center">Empleados</h1>    
    <section class="boxed padding">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="<c:url value='/registro' />">Registro empleado</a></li>
            <li role="presentation" class="active"><a href="#">Listado empleados</a></li>                        
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane padding active" id="pestana1">
                <div class="form-msg bottom-space ${msgType}" role="alert">${msg}</div>
                <table id="tablaEmpleado" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Primer Apellido</th>
                            <th>Segundo Apellido</th>
                            <th>Primer Nombre</th>
                            <th>Otros Nombres</th>
                            <th>Pais de empleo</th>
                            <th>Tipo identificacion</th>
                            <th>Numero identificacion</th>
                            <th>Correo</th>
                            <th>Fecha de ingreso</th>
                            <th>Area</th>
                            <th>Fecha de registro</th>
                            <th>Fecha de edicion</th>
                            <th>Estado</th>
                            <th>Editar</th>
                            <th>Eliminar/Restaurar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${lstEmpleado != null && lstEmpleado.size() > 0}">
                                <c:forEach items="${lstEmpleado}" var="empleado">
                                    <tr>
                                        <td>${empleado.primerApellido}</td>
                                        <td>${empleado.segundoApellido}</td>
                                        <td>${empleado.primerNombre}</td>
                                        <td>${empleado.otroNombre}</td>
                                        <td>${empleado.paisEmpleo}</td>
                                        <td>${empleado.tipoIdentificacion}</td>
                                        <td>${empleado.numeroIdentificacion}</td>
                                        <td>${empleado.correo}</td>                                    
                                        <td>${empleado.fechaIngreso}</td>
                                        <td>${empleado.area}</td>
                                        <td>${empleado.fechaHoraRegistro}</td>
                                        <td>${empleado.fechaHoraEdicion}</td>
                                        <c:choose>
                                            <c:when test="${empleado.estado == 1}">
                                                <td><span class="label label-success">Activo</span></td>
                                                <td>
                                                    <form class="form-in-table" action="<c:url value='/editaEmpleado' />" method="post">
                                                        <input type="hidden" name="idEmpleado" value="${empleado.idEmpleado}">
                                                        <input type="submit" class="btn btn-success btn-info btn-xs" value="Editar" style="width: 80px;">
                                                    </form>
                                                </td>
                                                <td>
                                                    <form id="form-elimina-empleado-${empleado.idEmpleado}" class="form-in-table" 
                                                          action="<c:url value='/eliminaEmpleado' />" method="post">
                                                        <input type="hidden" name="idEmpleado" value="${empleado.idEmpleado}">
                                                        <button type="button" class="btn btn-danger btn-xs" 
                                                            onclick="eliminaEmpleado('${empleado.primerNombre}',
                                                                                     '${empleado.primerApellido}',
                                                                                     '${empleado.idEmpleado}');">Eliminar</button>
                                                    </form>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <td><span class="label label-danger">Inactivo</span></td>
                                                <td><span class="label label-danger">No editable</span></td>
                                                <td>
                                                    <form class="form-in-table" action="<c:url value='/restauraEmpleado' />" method="post">
                                                        <input type="hidden" name="idEmpleado" value="${empleado.idEmpleado}">
                                                        <input type="submit" class="btn btn-success btn-xs" value="Restaurar" style="width: 80px;">
                                                    </form>
                                                </td>
                                            </c:otherwise>
                                        </c:choose>                                        
                                    </tr>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </tbody>
                </table>
            </div> 
        </div>            
    </section>
</div>

<jsp:include page="/include/footer.jsp" />

<script>
    // Tabla dinamica de empleados
    $(document).ready(function () {        
        $('#tablaEmpleado').DataTable({            
            responsive: true,
            aLengthMenu: [10, 50],
            scrollY: 600,
            searching: true,
            bAutoWidth: true,
            bInfo: false,
            paging: true,
            language: {url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"}
        });
    });
</script>

<jsp:include page="/include/end.jsp" />
