<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/include/header.jsp" />

<div class="col-lg-9 centered">
    <h1 class="display-3 bg-info text-center">Empleados</h1>    
    <section class="boxed padding"> 
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#">Registro empleado</a></li>
            <li role="presentation"><a href="<c:url value='/empleados' />">Listado empleados</a></li>
        </ul>
        <div class="tab-content centered">
            <div role="tabpanel" class="tab-pane padding active">                    

                <form role="form" id="form-registra-empleado" class="form-horizontal" action="<c:url value='/registraEmpleado' />" method="post">
                    <div id="msg" class="form-msg bottom-space ${msgType}" role="alert">${msg}</div>                    
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Primer Apellido</label>
                        <div class="controls">
                            <input type="text" name="papellido" id="papellido" maxlength="20" title="Digite su primer apellido" class="form-control" required>
                            <span id="msg_papellido"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Segundo Apellido</label>
                        <div class="controls">
                            <input type="text" name="sapellido" id="sapellido" maxlength="20" title="Digite su segundo apellido" class="form-control" required>
                            <span id="msg_sapellido"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Primer Nombre</label>
                        <div class="controls">
                            <input type="text" name="pnombre" id="pnombre" maxlength="20" title="Digite su primer nombre" class="form-control" required>
                            <span id="msg_pnombre"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">Otros Nombres</label>
                        <div class="controls">
                            <input type="text" name="snombre" id="snombre" maxlength="50" title="Digite su otro(s) nombre(s)" class="form-control">
                            <span id="msg_snombre"></span>
                        </div>
                    </div>                    
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Pa&iacute;s de empleo</label>
                        <div class="controls bottom-space">
                            <select id="pais" name="pais" class="form-control" title="Seleccione un pa&iacute;s">
                                <option value="">Seleccione un pa&iacute;s</option>
                                <option value="co">Colombia</option>
                                <option value="us">Estados Unidos</option>
                            </select>
                            <span id="msg_pais"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Tipo de Identificaci&oacute;n</label>
                        <div class="controls bottom-space">
                            <select name="tipoid" id="tipoid" class="form-control" title="Seleccione un tipo de documento">
                                <option value="">Seleccione un tipo de documento</option>
                                <option value="CC">Cedula de ciudadania</option>
                                <option value="CE">Cedula de extranjeria</option>
                                <option value="PA">Pasaporte</option>
                                <option value="PE">Permiso especial</option>                                
                            </select>
                            <span id="msg_tipoid"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">* N&uacute;mero de Identificaci&oacute;n</label>
                        <div class="controls">
                            <input type="text" name="numid" id="numid" maxlength="20" title="Digite su n&uacute;mero de identificaci&oacute;n" class="form-control" required>
                            <span id="msg_numid"></span>
                        </div>
                    </div>                     
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Correo electr&oacute;nico</label>
                        <div class="controls">
                            <input type="text" class="form-control" name="correo" id="correo" maxlength="300" readonly="readonly" required>
                            <span id="msg_correo"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">* Fecha de ingreso</label>
                        <div class="controls">
                            <input type="text" class="form-control" name="fechaIngreso" id="fechaIngreso" readonly="readonly" required>
                            <span id="msg_fechaIngreso"></span>
                        </div>
                    </div>
                    <div class="control-group col-sm-7">
                        <label class="control-label">* &Aacute;rea</label>
                        <div class="controls bottom-space">
                            <select id="area" name="area" class="form-control" title="Seleccione un &aacute;rea">
                                <option value="">Seleccione un &aacute;rea</option>
                                <option value="Administracion">Administracion</option>
                                <option value="Financiera">Financiera</option>
                                <option value="Compras">Compras</option>
                                <option value="Infraestructura">Infraestructura</option>
                                <option value="Operacion">Operacion</option>
                                <option value="Talento Humano">Talento Humano</option>
                                <option value="Servicios Varios">Servicios Varios</option>
                            </select>
                            <span id="msg_area"></span>                            
                        </div>
                    </div>                    
                    <div class="form-group">                                                                                    
                        <div class="col-sm-offset-2 col-sm-7" style="padding-top: 10px; margin-left: 15px;">
                            <button style="display:none;" type="submit" class="btn"></button>
                            <button style="width:126px;" type="button" class="btn" title="Haga click para registrar" onclick="registraEmpleado();">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>        
    </section>
</div>

<jsp:include page="/include/footer.jsp" />
<script>
    $(document).ready(function() {    
        
        var date = new Date();
        var prevMonth = date.agregarDias(-31);        
        
        var fechaIngreso = $('#fechaIngreso').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,            
            startDate: prevMonth,
            endDate: new Date(),
            language: 'es'
        }).on('changeDate', function (e) {
            fechaIngreso.datepicker('hide');
        });
        
        // Asigna evento en elementos involucrados en generacion de correo
        eventoGeneracionCorreo();
    });
</script>
<jsp:include page="/include/end.jsp" />

