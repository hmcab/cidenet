
var TEXT     = 1;
var TEXT_OPTIONAL = 2;
var SELECT   = 3;

var nombre  = 0, 
    tipo    = 1, 
    patron  = 2, 
    long    = 3, 
    msg     = 4;

var exp_alfanumerico_esp = /^([a-zA-Z0-9-_]+\s*)+$/;
var exp_alfanumerico = /^([a-zA-Z0-9]+\s*)+$/;
var exp_numerico     = /^[0-9]+$/;
var exp_alfabeto     = /^([a-zA-Z]+\s*)+$/;
var exp_alfabeto_noesp  = /^[a-zA-Z]+$/;
var exp_alfabeto_opt = /^([a-zA-Z]+\s*)*$/;
var exp_numid        = /^[a-zA-Z0-9-]+$/;
var exp_email        = /^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/;
var exp_alfanumerico_noesp  = /^[a-zA-Z0-9-]+$/;
var exp_fecha        = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;

// Valida cada campo especificado para registro/edicion de empleado,
// de ser alguno incorrecto, se notifica al usuario y se retorna valor falso,
// de lo contrario se retorna verdadero.
function vdt_validaEmpleado() {
    
    var campos = [
        // nombre campo, tipo, patron, longitud, msg error
        ['papellido',    TEXT, exp_alfabeto, 20,  '* &Uacute;nicamente caracteres del alfabeto. No se admiten tildes ni caracteres especiales.'],
        ['sapellido',    TEXT, exp_alfabeto, 20,  '* &Uacute;nicamente caracteres del alfabeto. No se admiten tildes ni caracteres especiales.' ],
        ['pnombre',      TEXT, exp_alfabeto_noesp, 20,  '* &Uacute;nicamente caracteres del alfabeto. No se admiten tildes ni caracteres especiales.' ],        
        ['snombre',      TEXT_OPTIONAL, exp_alfabeto, 50,  '* &Uacute;nicamente caracteres del alfabeto. No se admiten tildes ni caracteres especiales.'],
        ['pais',         SELECT, "", null,  '* Especifique un pais.'],
        ['tipoid',       SELECT, "", null,  '* Especifique un tipo de documento.'],
        ['numid',        TEXT, exp_numid,   50,  '* &Uacute;nicamente caracteres alfanum&eacute;ricos y s&iacute;mbolo -.'],
        ['correo',       TEXT, exp_email,   300, '* Especifique un correo electr&oacute;nico v&aacute;lido.'],
        ['fechaIngreso', TEXT, exp_fecha,   10,  '* Especifique una fecha.'],
        ['area',         SELECT, "", null,  '* Especifique un &Aacute;rea.']
    ];
    
    return vdt_valida(campos);
}

// Itera sobre cada campo del arreglo especificado (campos), 
// para el que se valida su contenido y tamaño segun
// el tipo que corresponda. De existir alguno incorrecto,
// se notifica al usuario y se retorna falso, de lo contrario
// se retorna verdadero.
function vdt_valida(campos) {    
    
    if (campos.length <= 0)
        return false;
    
    for (var i = 0; i < campos.length; i++) {
        
        var comp = document.getElementById(campos[i][nombre]);  
        
        if (comp != null) {

            var msg_ = document.getElementById("msg_" + campos[i][nombre]);
            var exp  = campos[i][patron];        

            msg_.innerHTML = "";
            msg_.setAttribute("class", "msg-error");            

            if (campos[i][tipo] == TEXT_OPTIONAL) {
                if (comp.value != "" && !exp.test(comp.value)) {
                    msg_.innerHTML = campos[i][msg];
                    return false;
                }
            }

            if (campos[i][tipo] == TEXT) {                
                if (comp.value == "" || !exp.test(comp.value)) {
                    msg_.innerHTML = campos[i][msg];
                    return false;
                }
                if (campos[i][long] != null && comp.value.length > campos[i][long]) {
                    msg_.innerHTML = "* Longitud m&aacute;xima hasta " + campos[i][long] + " caracteres.";
                    return false;
                }
            }

            if (campos[i][tipo] == SELECT) {
                if (comp.value == null || comp.value == exp) {
                    msg_.innerHTML = campos[i][msg];
                    return false;
                }
            }
        }
    } 
    return true;
}