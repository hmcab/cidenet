
package com.cidenet.persistencia;

import com.cidenet.logica.Empleado;
import com.cidenet.util.Response;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/*
    Hector M Cabrera
*/

public class DataEmpleadoTest {
    
    public DataEmpleadoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    public Empleado empleado1() {
        Empleado emp = new Empleado();
        emp.setPrimerNombre("jose");
        emp.setOtroNombre("julian");
        emp.setPrimerApellido("de la calle");        
        emp.setSegundoApellido("lopez");
        emp.setTipoIdentificacion("CC");
        emp.setNumeroIdentificacion("12345");
        emp.setPaisEmpleo("co");
        emp.setArea("Administracion");
        emp.setFechaIngreso("10/01/2020");
        emp.setEstado(1);         
        ValidaEmpleado.asignaId(emp);
        ValidaEmpleado.generaCorreo(emp);        
        return emp;
    }
    
    public Empleado empleado2() {
        Empleado emp = new Empleado();
        emp.setPrimerNombre("jose");
        emp.setOtroNombre("andres");
        emp.setPrimerApellido("de la calle");        
        emp.setSegundoApellido("cruz");
        emp.setTipoIdentificacion("CC");
        emp.setNumeroIdentificacion("456789");
        emp.setPaisEmpleo("us");
        emp.setArea("Infraestructura");
        emp.setFechaIngreso("12/01/2020");
        emp.setEstado(1);        
        ValidaEmpleado.asignaId(emp);
        ValidaEmpleado.generaCorreo(emp);        
        return emp;
    }
    
    public Empleado empleado3() {
        Empleado emp = new Empleado();
        emp.setPrimerNombre("jose");
        emp.setOtroNombre("andres");
        emp.setPrimerApellido("de la rua");        
        emp.setSegundoApellido("jimenez");
        emp.setTipoIdentificacion("CC");
        emp.setNumeroIdentificacion("777");
        emp.setPaisEmpleo("us");
        emp.setArea("Talento Humano");
        emp.setFechaIngreso("14/01/2020");
        emp.setEstado(1);        
        ValidaEmpleado.asignaId(emp);
        ValidaEmpleado.generaCorreo(emp);        
        return emp;
    }    
    
    public Empleado empleadoIncorrecto() {
        Empleado emp = new Empleado();
        emp.setPrimerNombre("william");
        emp.setOtroNombre("andres");
        emp.setPrimerApellido("parra");        
        emp.setSegundoApellido("zambrano $%&###");
        emp.setTipoIdentificacion("CC");
        emp.setNumeroIdentificacion("10001 $$%&1");
        emp.setPaisEmpleo("us");
        emp.setArea("Talento Humano");
        emp.setFechaIngreso("14/01/2020");
        emp.setEstado(1);
        ValidaEmpleado.asignaId(emp);
        ValidaEmpleado.generaCorreo(emp);        
        return emp;
    }    
    
    public void testRegistrar() {
        System.out.println("registrar");
        Empleado empleado = empleado1();
        Response expResult = new Response();
        expResult.setResult(true);
        Response result = DataEmpleado.registrar(empleado);
        assertEquals(expResult.getResult(), result.getResult());
        assertEquals(DataEmpleado.size(), 1);
    }
    
    public void testRegistrarDuplicado() {
        System.out.println("registrar duplicado");
        Empleado empleado = empleado1();
        Response expResult = new Response();
        expResult.setResult(false);
        Response result = DataEmpleado.registrar(empleado);
        assertEquals(expResult.getResult(), result.getResult());
        assertEquals(DataEmpleado.size(), 1);
    }
        
    public void testEliminar() {
        System.out.println("eliminar");
        Empleado empleado = empleado1();
        String id = empleado.getIdEmpleado();
        Response expResult = new Response();
        expResult.setResult(true);
        Response result = DataEmpleado.eliminar(id);
        Response resultDel = DataEmpleado.consultaEmpleado(id);
        Empleado empleadoDel = (Empleado) resultDel.getObject();               
        
        assertEquals(expResult.getResult(), result.getResult());
        assertEquals(empleadoDel.getEstado(), 0);
        assertEquals(DataEmpleado.size(), 1);
    }
    
    public void testRestaurar() {
        System.out.println("restaurar");
        Empleado empleado = empleado1();
        String id = empleado.getIdEmpleado();
        Response expResult = new Response();
        expResult.setResult(true);
        Response result = DataEmpleado.restaurar(id);
        Response resultRest = DataEmpleado.consultaEmpleado(id);
        Empleado empleadoRest = (Empleado) resultRest.getObject();               
        
        assertEquals(expResult.getResult(), result.getResult());
        assertEquals(empleadoRest.getEstado(), 1);
        assertEquals(DataEmpleado.size(), 1);
    }
    
    public void testActualizar() {
        System.out.println("actualizacion");
        Empleado empleado = empleado1();                    
        System.out.println("Antes de actualizacion: ");
        System.out.println(empleado.getCorreo());
        System.out.println(empleado.getPaisEmpleo() +"/"+ empleado.getDominioPaisEmpleo());
        System.out.println(empleado.getArea());
        
        empleado.setPaisEmpleo("us");
        empleado.setArea("Operacion");
        ValidaEmpleado.generaCorreo(empleado);
        
        String id = empleado.getIdEmpleado();
        Response expResult = new Response();
        expResult.setResult(true);
        Response result = DataEmpleado.actualizar(id, id, empleado);
        Response resultUpd = DataEmpleado.consultaEmpleado(id);
        Empleado empleadoUpd = (Empleado) resultUpd.getObject();
        
        System.out.println("\nDespues de actualizacion: ");
        System.out.println(empleadoUpd.getCorreo());
        System.out.println(empleadoUpd.getPaisEmpleo() +"/"+ empleado.getDominioPaisEmpleo());
        System.out.println(empleadoUpd.getArea());
                
        assertEquals(expResult.getResult(), result.getResult());
        assertEquals(empleadoUpd.getArea(), "Operacion");
        assertEquals(empleadoUpd.getDominioPaisEmpleo(), "us");
        assertEquals(empleadoUpd.getPaisEmpleo(), "Estados Unidos");
    }   
    
    public void testValidacionCorrecta() {
        Empleado empCorrecto = empleado2();
        boolean result = ValidaEmpleado.valida(empCorrecto);
        assertEquals(result, true);
    }
    
    public void testValidacionIncorrecta() {
        Empleado empIncorrecto = empleadoIncorrecto();
        boolean result = ValidaEmpleado.valida(empIncorrecto);
        assertEquals(result, false);
    }
    
    public void testDuplicacionCorreo() {
        Empleado emp = empleado3();
        Empleado emp1 = empleado3();
        Empleado emp2 = empleado3();
        Empleado emp3 = empleado3();
        emp1.setNumeroIdentificacion("888");
        emp2.setNumeroIdentificacion("999");
        emp3.setNumeroIdentificacion("000");
        ValidaEmpleado.valida(emp);
        ValidaEmpleado.valida(emp1);
        ValidaEmpleado.valida(emp2);
        ValidaEmpleado.valida(emp3);
        DataEmpleado.registrar(emp);
        DataEmpleado.registrar(emp1);
        DataEmpleado.registrar(emp2);
        DataEmpleado.registrar(emp3);
        Response resp  = DataEmpleado.consultaEmpleado(emp.getIdEmpleado());
        Response resp1 = DataEmpleado.consultaEmpleado(emp1.getIdEmpleado());
        Response resp2 = DataEmpleado.consultaEmpleado(emp2.getIdEmpleado());
        Response resp3 = DataEmpleado.consultaEmpleado(emp3.getIdEmpleado());
        Empleado empReg  = (Empleado) resp.getObject();
        Empleado empReg1 = (Empleado) resp1.getObject();
        Empleado empReg2 = (Empleado) resp2.getObject();
        Empleado empReg3 = (Empleado) resp3.getObject();
        assertEquals(empReg.getCorreo(),  "jose.delarua@cidenet.com.us");
        assertEquals(empReg1.getCorreo(), "jose.delarua.1@cidenet.com.us");
        assertEquals(empReg2.getCorreo(), "jose.delarua.2@cidenet.com.us");
        assertEquals(empReg3.getCorreo(), "jose.delarua.3@cidenet.com.us");
    }
        
    @Test
    public void test1() {
        testRegistrar();
    }
    
    @Test 
    public void test2() {        
        testRegistrarDuplicado();
    }
    
    @Test
    public void test3() {
        testEliminar();
    }
    
    @Test
    public void test4() {
        testRestaurar();
    }
    
    @Test
    public void test5() {
        testActualizar();
    }
    
    @Test
    public void test6() {
        testValidacionCorrecta();
    }    
    
    @Test
    public void test7() {
        testValidacionIncorrecta();
    }
    
    @Test
    public void test8() {
        testDuplicacionCorreo();
    }
    
}
