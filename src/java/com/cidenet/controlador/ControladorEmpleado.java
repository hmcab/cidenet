
package com.cidenet.controlador;

import com.cidenet.logica.Empleado;
import com.cidenet.persistencia.DataEmpleado;
import com.cidenet.persistencia.ValidaEmpleado;
import com.cidenet.util.Response;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControladorEmpleado extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {    
                
        String requestURI = request.getRequestURI();
        String url = "/app/empleado/listado.jsp";
        
        if (requestURI.endsWith("/registro")) {
            url = "/app/empleado/registro.jsp";
        } else if (requestURI.endsWith("/empleados")) {
            request.setAttribute("lstEmpleado", DataEmpleado.obtenerLista());
        }
        
        getServletContext()
            .getRequestDispatcher(url)
            .forward(request, response);
    }
    
    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        
        String requestURI = request.getRequestURI();
        String url = "/app/listado.jsp";
        
        if (requestURI.endsWith("/registraEmpleado")) {
            url = registraEmpleado(request, response);
        } else if (requestURI.endsWith("/editaEmpleado")) {
            url = editaEmpleado(request, response);
        } else if (requestURI.endsWith("/actualizaEmpleado")) {
            url = actualizaEmpleado(request, response);
        } else if (requestURI.endsWith("/eliminaEmpleado")) {
            url = eliminaEmpleado(request, response);
        } else if (requestURI.endsWith("/restauraEmpleado")) {
            url = restauraEmpleado(request, response);
        }                
        
        getServletContext()
            .getRequestDispatcher(url)
            .forward(request, response);
    }    
    
    public String registraEmpleado(HttpServletRequest request, HttpServletResponse response) {
        
        SimpleDateFormat fhfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaHoraRegistro = fhfmt.format(new Date());
        
        Empleado empleado = new Empleado();
        empleado.setPrimerApellido(request.getParameter("papellido"));
        empleado.setSegundoApellido(request.getParameter("sapellido"));
        empleado.setPrimerNombre(request.getParameter("pnombre"));
        empleado.setOtroNombre(request.getParameter("snombre"));        
        empleado.setPaisEmpleo(request.getParameter("pais"));
        empleado.setTipoIdentificacion(request.getParameter("tipoid"));
        empleado.setNumeroIdentificacion(request.getParameter("numid"));        
        empleado.setFechaIngreso(request.getParameter("fechaIngreso"));
        empleado.setArea(request.getParameter("area"));
        empleado.setFechaHoraRegistro(fechaHoraRegistro);
        empleado.setFechaHoraEdicion(fechaHoraRegistro);
        empleado.setEstado(1);
                        
        if (!ValidaEmpleado.valida(empleado)) {            
            request.setAttribute("msg", "* Empleado no registrado. Verifique sus datos.");
            request.setAttribute("msgType", "alert alert-danger");
            return "/app/empleado/registro.jsp";
        }
        
        Response resp  = DataEmpleado.registrar(empleado);
        String msgType = (resp.getResult()) ? "alert alert-success" : "alert alert-info";        
        request.setAttribute("msg", "* " + resp.getMessage());
        request.setAttribute("msgType", msgType);
        
        return "/app/empleado/registro.jsp";
    }
    
    public String editaEmpleado(HttpServletRequest request, HttpServletResponse response) {
        
        String idEmpleado = request.getParameter("idEmpleado");
        Response resp = DataEmpleado.consultaEmpleado(idEmpleado);
        
        if (resp.getResult()) {            
            request.setAttribute("empleado", resp.getObject());
        } else {
            request.setAttribute("msg", "* " + resp.getMessage());
            request.setAttribute("msgType", "alert alert-info");
            return "/app/empleado/listado.jsp";
        }
        
        return "/app/empleado/edicion.jsp";
    }
    
    public String actualizaEmpleado(HttpServletRequest request, HttpServletResponse response) {
        
        SimpleDateFormat fhfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaHoraEdicion = fhfmt.format(new Date());
        
        Empleado empleado = new Empleado();        
        empleado.setPrimerApellido(request.getParameter("papellido"));
        empleado.setSegundoApellido(request.getParameter("sapellido"));
        empleado.setPrimerNombre(request.getParameter("pnombre"));
        empleado.setOtroNombre(request.getParameter("snombre"));        
        empleado.setPaisEmpleo(request.getParameter("pais"));
        empleado.setTipoIdentificacion(request.getParameter("tipoid"));
        empleado.setNumeroIdentificacion(request.getParameter("numid"));
        empleado.setFechaIngreso(request.getParameter("fechaIngreso"));
        empleado.setArea(request.getParameter("area"));
        empleado.setFechaHoraRegistro(request.getParameter("fechaHoraRegistro"));
        empleado.setFechaHoraEdicion(fechaHoraEdicion);
        empleado.setEstado(1);        
                        
        if (!ValidaEmpleado.valida(empleado)) {            
            request.setAttribute("msg", "* Empleado no actualizado. Verifique sus datos.");
            request.setAttribute("msgType", "alert alert-danger");
            return "/app/empleado/registro.jsp";
        }
        
        String idActual = request.getParameter("idEmpleado");
        String idNuevo  = empleado.getIdEmpleado();
        
        Response resp  = DataEmpleado.actualizar(idNuevo, idActual, empleado);
        String msgType = (resp.getResult()) ? "alert alert-success" : "alert alert-info";        
        request.setAttribute("msg", "* " + resp.getMessage());
        request.setAttribute("msgType", msgType);
        
        return "/app/empleado/edicion.jsp";
    }
    
    public String eliminaEmpleado(HttpServletRequest request, HttpServletResponse response) {
        
        String idEmpleado = request.getParameter("idEmpleado");
        Response resp  = DataEmpleado.eliminar(idEmpleado);
        String msgType = (resp.getResult()) ? "alert alert-success" : "alert alert-danger";
        
        request.setAttribute("msg", "* " + resp.getMessage());
        request.setAttribute("msgType", msgType);
        request.setAttribute("lstEmpleado", DataEmpleado.obtenerLista());
        
        return "/app/empleado/listado.jsp";
    }
    
    public String restauraEmpleado(HttpServletRequest request, HttpServletResponse response) {
        
        String idEmpleado = request.getParameter("idEmpleado");
        Response resp  = DataEmpleado.restaurar(idEmpleado);
        String msgType = (resp.getResult()) ? "alert alert-success" : "alert alert-danger";
        
        request.setAttribute("msg", "* " + resp.getMessage());
        request.setAttribute("msgType", msgType);
        request.setAttribute("lstEmpleado", DataEmpleado.obtenerLista());
        
        return "/app/empleado/listado.jsp";
    }    
}
