
package com.cidenet.logica;

import java.util.HashMap;
import java.util.Map;

public class TipoIdentificacion {
    
    Map<String,String> tipoId = null;
    
    public TipoIdentificacion() {
        tipoId = new HashMap<String,String>();
        tipoId.put("CC", "Cedula de ciudadania");
        tipoId.put("CE", "Cedula de extranjeria");
        tipoId.put("PA", "Pasaporte");
        tipoId.put("PE", "Permiso especial");        
    }
    
    public String getNombreTipoDocumento(String id) {
        return tipoId.get(id);
    }  
    
}
