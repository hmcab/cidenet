
package com.cidenet.logica;

/*
    Hector M Cabrera
*/
public class Empleado {
    
    private static int idNumber = 0;
    private String idEmpleado;
    private String primerNombre;
    private String otroNombre;
    private String primerApellido;
    private String segundoApellido;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String dominioPaisEmpleo;
    private String paisEmpleo;
    private String correo;
    private String area;
    private String fechaIngreso;
    private String fechaHoraRegistro;
    private String fechaHoraEdicion;
    private int estado;
    
    public static int generateId() {
        idNumber += 1;
        return idNumber;
    }        
    
    public void setIdEmpleado(String idEmpleado) {
        this.idEmpleado = idEmpleado;
    }
    
    public String getIdEmpleado() {
        return idEmpleado;
    }
    
    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        if (primerNombre != null)
            this.primerNombre = primerNombre.toUpperCase();        
    }

    public String getOtroNombre() {        
        return otroNombre;
    }

    public void setOtroNombre(String otroNombre) {
        if (otroNombre != null)
            this.otroNombre = otroNombre.toUpperCase();
        else
            this.otroNombre = "";
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        if (primerApellido != null)
            this.primerApellido = primerApellido.toUpperCase();
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        if (segundoApellido != null)
            this.segundoApellido = segundoApellido.toUpperCase();
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        if (tipoIdentificacion != null)
            this.tipoIdentificacion = tipoIdentificacion.toUpperCase();
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        if (numeroIdentificacion != null)
            this.numeroIdentificacion = numeroIdentificacion.toUpperCase();
    }

    public String getPaisEmpleo() {
        return paisEmpleo;
    }

    public void setPaisEmpleo(String dominioPais) {
        Pais pais = new Pais();
        this.paisEmpleo = pais.getNombrePais(dominioPais);
        this.dominioPaisEmpleo = dominioPais;
    }
    
    public String getDominioPaisEmpleo() {
        return dominioPaisEmpleo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(String fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public String getFechaHoraEdicion() {
        return fechaHoraEdicion;
    }

    public void setFechaHoraEdicion(String fechaHoraEdicion) {
        this.fechaHoraEdicion = fechaHoraEdicion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }      
    
    public String toString() {
        return 
                "primer apellido: " + this.primerApellido + "\n" +
                "segundo apellido: " + this.segundoApellido + "\n" +
                "primer nombre: " + this.primerNombre + "\n" +
                "otros nombres: " + this.otroNombre   + "\n" +             
                "tipo documento: " + this.tipoIdentificacion + "\n" +
                "numero documento: " + this.numeroIdentificacion + "\n" +                
                "pais: " + this.paisEmpleo + "\n" +
                "dominio: " + this.dominioPaisEmpleo + "\n" +
                "correo: " + this.correo + "\n" +
                "area: " + this.area + "\n" +
                "fecha: " + this.fechaIngreso + "\n" +
                "estado: " + this.estado;
    }
}
