
package com.cidenet.logica;

import java.util.HashMap;
import java.util.Map;

public class Pais {
    
    Map<String,String> pais = null;
    
    public Pais() {
        pais = new HashMap<String,String>();
        pais.put("co", "Colombia");
        pais.put("us", "Estados Unidos");
    }
    
    public String getNombrePais(String id) {
        return pais.get(id);
    }    
}
