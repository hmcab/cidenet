
package com.cidenet.persistencia;

import com.cidenet.logica.Empleado;
import com.cidenet.util.Validate;

public class ValidaEmpleado {
    
    public static boolean asignaId(Empleado empleado) {
        
        String tipoId = empleado.getTipoIdentificacion();
        String numId  = empleado.getNumeroIdentificacion();
        
        if (tipoId != null && numId != null) {
            String empId = tipoId.toUpperCase() + "-" + numId.toUpperCase();
            empleado.setIdEmpleado(empId);
            return true;
        }
        return false;
    }
    
    public static boolean generaCorreo(Empleado empleado) {
        
        String nombre   = empleado.getPrimerNombre();
        String apellido = empleado.getPrimerApellido();
        String dominio  = empleado.getDominioPaisEmpleo();
        String correo   = null;
        
        if (nombre != null && apellido != null && dominio != null) {
            nombre   = nombre.toLowerCase();
            apellido = apellido.toLowerCase();
            dominio  = dominio.toLowerCase();
            apellido = apellido.replace(" ", "");
            correo   = nombre + "." + apellido + "@cidenet.com." + dominio;
            empleado.setCorreo(correo);            
            return true;
        }
        return false;
    }
    
    public static boolean valida(Empleado empleado) {
        return (asignaId(empleado) &&
                generaCorreo(empleado) &&
                Validate.isAlpha(empleado.getPrimerNombre()) &&
                (empleado.getOtroNombre().equals("") ||
                Validate.isAlpha_esp(empleado.getOtroNombre())) &&
                Validate.isAlpha_esp(empleado.getPrimerApellido()) &&
                Validate.isAlpha_esp(empleado.getSegundoApellido()) &&
                Validate.isAlpha(empleado.getDominioPaisEmpleo()) &&
                Validate.isAlpha_esp(empleado.getPaisEmpleo()) &&
                Validate.isAlpha(empleado.getTipoIdentificacion()) &&
                Validate.isAlphanumeric(empleado.getNumeroIdentificacion()) &&
                Validate.isAlpha_esp(empleado.getArea()) &&                
                Validate.isEmail(empleado.getCorreo()) &&            
                Validate.isDate(empleado.getFechaIngreso()));
    }
}
