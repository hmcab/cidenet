
package com.cidenet.persistencia;

import com.cidenet.logica.Empleado;
import com.cidenet.util.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
    Hector M Cabrera
*/

public class DataEmpleado {
    
    private static Map<String,Empleado> data = new HashMap<String,Empleado>();
    
    public static Response registrar(Empleado empleado) {
        
        String id = empleado.getIdEmpleado();
        Empleado empleadoG = data.get(id);
        Response resp = new Response();
        
        if (empleadoG != null) {
            resp.setResult(false);
            resp.setMessage("Empleado identificado con " + id + " ya existe.");
        } else {       
            empleado.setCorreo(ajustaCorreo(empleado));
            data.put(id,empleado);
            resp.setResult(true);
            resp.setMessage("Empleado registrado correctamente.");
        }
        return resp;
    }   
    
    public static Response eliminaRestaura(String id, int estado) {
        
        Empleado empleadoG = data.get(id);
        Response resp  = new Response();
        String sestado = (estado == 1) ? "restaurado" : "eliminado";
        
        if (empleadoG != null) {
            empleadoG.setEstado(estado);
            resp.setResult(true);
            resp.setMessage("Empleado " + sestado + " correctamente.");
        } else {
            resp.setResult(false);
            resp.setMessage("Empleado identificado con " + id + " no existe.");
        }
        return resp;
    }
    
    public static Response eliminar(String id) {
        return eliminaRestaura(id, 0);
    }
    
    public static Response restaurar(String id) {
        return eliminaRestaura(id, 1);
    }    
    
    public static Response actualizar(String idNuevo, String idActual, Empleado empleado) {
        Response resp = new Response();
        empleado.setCorreo(ajustaCorreo(empleado));
        if (idNuevo.equals(idActual)) {            
            data.put(idActual, empleado);
        } else {
            data.remove(idActual);
            data.put(idNuevo, empleado);
        }
        resp.setResult(true);
        resp.setMessage("Empleado actualizado correctamente.");
        return resp;
    }
    
    public static String ajustaCorreo(Empleado empleado) {
        String idEmpleado = empleado.getIdEmpleado();
        String nombre     = empleado.getPrimerNombre().toLowerCase();
        String apellido   = empleado.getPrimerApellido().toLowerCase();
        String dominio    = empleado.getDominioPaisEmpleo().toLowerCase();        
        String correo     = empleado.getCorreo();
        apellido          = apellido.replace(" ", "");
        if (existeCorreo(idEmpleado, correo)) {
            int id = Empleado.generateId();
            correo = nombre + "." + apellido + "." + id + "@cidenet.com." + dominio;
        }
        return correo;
    }
    
    public static boolean existeCorreo(String idEmpleado, String correo) {
        
        for (Map.Entry<String,Empleado> entry : data.entrySet()) {
            Empleado empleado = entry.getValue();
            if (empleado.getIdEmpleado().equals(idEmpleado)) {
                continue;
            }
            if (correo.equals(empleado.getCorreo())) {
                return true;
            }
        }
        return false;
    }
        
    public static ArrayList<Empleado> obtenerLista() {
        ArrayList<Empleado> lst = new ArrayList<Empleado>();
        for (Map.Entry<String,Empleado> entry : data.entrySet()) {
            lst.add(entry.getValue());
        }
        return lst;
    }
    
    public static Response consultaEmpleado(String id) {
        Response resp = new Response();
        Empleado empleado = data.get(id);
        if (empleado != null) {
            resp.setResult(true);
            resp.setObject(empleado);
        } else {
            resp.setResult(false);
            resp.setMessage("Empleado identificado con " + id + " no existe.");
        }
        return resp;
    }
    
    public static int size() {
        return data.size();
    }
    
}
