
package com.cidenet.util;

import java.util.UUID;

public class Validate  {
    
    private static final String EMAIL_LOGIN_PATTERN = 
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*";
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            
    // Es numero entero Ej. 12/-12 (sin signo +)
    public static boolean isNumber (String s) {
        if (s != null && s.matches("^-?[0-9]+$"))
            return true;
        return false;
    }    
    
    // Es alfabetico
    public static boolean isAlpha (String s) {
        if (s != null && s.matches("^[a-zA-Z]+$"))
            return true;
        return false;
    }
    
    // Es alfanumerico
    public static boolean isAlphanumeric (String s) {
        if (s != null && s.matches("^[a-zA-Z0-9]+$"))
            return true;
        return false;
    }
    
    // Es alfabetico con espacios
    public static boolean isAlpha_esp (String s) {
        if (s != null && s.matches("^([a-zA-Z]+\\s*)+$"))
            return true;
        return false;
    }
    
    // Es alfanumerico con espacios
    public static boolean isAlphanumeric_esp (String s) {
        if (s != null && s.matches("^([a-zA-Z0-9]+\\s*)+$"))
            return true;
        return false;
    }
    
    // Es fecha en formato dd/mm/yyyy
    public static boolean isDate (String s) {
        if (s != null && s.matches("^[0-9]{2,2}/[0-9]{2,2}/[0-9]{4,4}$"))
            return true;
        return false;
    }
    
    // Es fecha-tiempo en formato dd/mm/yyyy hh:mm:ss
    public static boolean isDateTime (String s) {
        if (s != null && s.matches("^[0-9]{2,2}/[0-9]{2,2}/[0-9]{4,4}\\s{1,1}[0-9]{2,2}:[0-9]{2,2}:[0-9]{2,2}$"))
            return true;
        return false;
    }
    
    // Es alfanumerico con simbolo -
    public static boolean isNamePoint (String s) {
        if (s != null && s.matches("^[a-zA-Z0-9-]+$"))
            return true;
        return false;
    }
    
    // Concuerda con expresion regular para correo
    public static boolean isEmail (String s) {
        if (s != null && s.matches(EMAIL_PATTERN))
            return true;
        return false;
    }
    
    // Concuerda con expresion regular para correo o cadena alfanumerica
    // con simbolos ._-
    public static boolean isEmailLogin (String s) {
        if (s != null && (s.matches(EMAIL_LOGIN_PATTERN) || s.matches(EMAIL_PATTERN)))
            return true;
        return false;
    }        
}