
package com.cidenet.util;

public class Response {
    
    private boolean result;
    private String message;
    private Object object;

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }       

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }        
}
