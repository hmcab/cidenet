<!DOCTYPE html>
<html lang="es">
    <head>        
        <title>Cidenet</title>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>        
    </head>
    <body>
        <div class="container">
            <div class="row top-space">                
                <div class="center-block alert alert-warning">
                    El servidor no obtuvo una respuesta para su solicitud. 
                    Por favor int&eacute;ntelo m&aacute;s tarde.
                    Para regresar al inicio pulse 
                    <a href="/Cidenet/index.jsp">aqu&iacute;</a>.
                </div>
            </div>            
        </div>
    </body>
</html>