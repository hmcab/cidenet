
function registraEmpleado() {
    if (vdt_validaEmpleado()) {
        var form = "#form-registra-empleado";
        // Accionar submit salta las restricciones de los campos
        // document.getElementById(form).submit();    
        $(form).find('[type="submit"]').trigger('click');        
    }
}

function actualizaEmpleado() {
    if (vdt_validaEmpleado()) {
        var form = "#form-actualiza-empleado";
        // Accionar submit salta las restricciones de los campos
        // document.getElementById(form).submit();    
        $(form).find('[type="submit"]').trigger('click');
    }
}

function eliminaEmpleado(nombre,apellido,id) {
    
    var sform = "#form-elimina-empleado-" + id;
    var msg = "¿Est&aacute; seguro de eliminar el siguiente empleado?<br><br>";
    msg += "<strong>Primer nombre:</strong> " + nombre + "<br>";
    msg += "<strong>Primer apellido:</strong> " + apellido + "<br>";
    msg += "<strong>Identificaci&oacute;n:</strong> " + id + "<br>";

    bootbox.confirm({
        title: "Eliminaci&oacute;n de registro",
        message: msg,
        buttons: {
            confirm: {
                label: 'Aceptar',
                className: 'btn btn-primary'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn'
            }
        },
        callback: function (rs) {
            if (rs) {
                $(sform).submit();
            }
        }
    });
}

// Ubica y selecciona opcion de componente select id_elemt
// cuyo valor de opcion es es igual a value.
function selectElement(id_elemt, value) {
    var sel = document.getElementById(id_elemt);
    if (sel !== null) {
        var options = sel.options;
        for (var i = 0; i < options.length; i++) {
            var optvalue = options[i].value;
            if (optvalue === value) {
                sel.options[i].selected = true;
                break;
            }
        }
    }
}

// Selecciona opcion especifica para cada select del arreglo elemts
function selectElements(elemts) {
    for (var i = 0; i < elemts.length; i++) {
        var elemt = elemts[i].elemt;
        var data_elemt = document.getElementById(elemts[i].data_elemt);
        if (elemt !== null && data_elemt !== null) {
            selectElement(elemt, data_elemt.value);
        }
    }
}

function eventoGeneracionCorreo() {
    document.getElementById("pnombre").addEventListener("blur", generaCorreo);
    document.getElementById("papellido").addEventListener("blur", generaCorreo);
    document.getElementById("pais").addEventListener("blur", generaCorreo);
    document.getElementById("correo").addEventListener("focus", generaCorreo);
}

function generaCorreo() {
    var nombre   = document.getElementById("pnombre");
    var apellido = document.getElementById("papellido");
    var pais     = document.getElementById("pais");    
    var correo   = "";
    
    if (nombre !== null && nombre.value !== "") {
        correo = nombre.value.toLowerCase();
    }
            
    if (apellido !== null && apellido.value !== "") {
        apellido = apellido.value.toLowerCase();
        apellido = apellido.replace(/\s/g, "");
        correo  += "." + apellido;
    }
       
    if (correo !== "") {
        correo += "@cidenet.com";
        if (pais !== null && pais.value !== "") {
            correo += "." + pais.value;
        }
    }    
    
    if (correo !== "") {
        document.getElementById("correo").value = correo;
    }
}

Date.prototype.agregarDias = function(dias) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + dias);
    return date;
}